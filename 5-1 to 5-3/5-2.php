<?php
include_once "5-3.html" ;
$prevMonth = $prevYear = $nextMonth = $nextYear = $currDate = "";

if(isset($_GET["month"]) && isset($_GET["year"]))
{
	if(!is_numeric($_GET["month"]) || !is_numeric($_GET["year"]) || !checkdate($_GET["month"], 1, $_GET["year"]))
		die("Invalid month or year format!");

	$currMonth = $_GET["month"];
	$info = cal_info(0);
	$currMonthName = $info["months"][ltrim($currMonth,"0")];
	$currYear = $_GET["year"];
	$daysInCurrMonth = cal_days_in_month(CAL_GREGORIAN, $currMonth, $currYear);

	if($currMonth == date("m") && $currYear == date("Y"))
		$currDate = date("d");
}
else
{
	$currDate = date("d");
	$currMonth = date("m");
	$currMonthName = date("F");
	$currYear = date("Y");
	$daysInCurrMonth = cal_days_in_month(CAL_GREGORIAN, $currMonth, $currYear);
}

if(($currMonth - 1) != 0)
{
	$prevMonth = str_pad(($currMonth - 1), 2, "0",STR_PAD_LEFT);
	$prevYear = $currYear;
}
else
{
	$prevMonth = "12";
	$prevYear = ($currYear - 1);
}

if(($currMonth + 1) != 13)
{
	$nextMonth = str_pad(($currMonth + 1), 2, "0",STR_PAD_LEFT);
	$nextYear = $currYear;
}
else
{
	$nextMonth = "01";
	$nextYear = ($currYear + 1);
}

$week = array("Su","Mo","Tu","We","Th","Fr","Sa");
/*echo "$currMonth<br>";
echo "$currDate<br>";
echo "$currYear<br>";
echo "$daysInCurrMonth<br>";*/
$calendar = "<div id='calendar' style='position: relative; width:250px; border: solid 1px #000; padding: 5px; text-align: center;'>";
$calendar .= "<a href='5-2.php?month=$prevMonth&year=$prevYear' style='position: absolute;top: 30px;left: 10px; text-decoration: none; border: solid 1px; border-radius: 100%; padding: 0 5px 0 5px;'><</a><h2 id='month_year_label'>$currMonthName $currYear</h2><a href='5-2.php?month=$nextMonth&year=$nextYear' style='position: absolute;top: 30px; right: 10px; text-decoration: none; border: solid 1px; border-radius: 100%; padding: 0 5px 0 5px;'>></a>";
$table = "<table style='margin:auto; text-align: center;'>";
$table .= "<thead><tr>";

foreach ($week as $wKey => $wVal)
	$table .= "<th>$wVal</th>";

$table .= "</tr></thead>";
$table .= "<tbody>";

$dayCount = 0;
$rows = "";
$row = "";
for ($i=1; $i <= $daysInCurrMonth; $i++)
{
	if($dayCount === 0)
		$row .= "<tr>";

	if($i === 1)
	{
		$day = date('w', strtotime("$currYear-$currMonth-$i"));

		if($day > 0)
		{
			$fillCount = 0;
			while ($fillCount < $day)
			{
				$row .= "<td></td>";
				$fillCount++;
				$dayCount++;
			}
		}
	}

	$highlight = ($i == $currDate) ? 'background-color: skyblue;' : '';
	$row .= "<td style='$highlight border: solid 1px;'>$i</td>";
	
	$dayCount++;
	if($dayCount === 7 || $i == $daysInCurrMonth)
	{
		$dayCount = 0;
		$row .= "</tr>";
		$rows .= $row;
		$row = "";
	}
}
$table .= $rows;
$table .= "</tbody>";
$table .= "</table>";
$calendar .= $table;
$calendar .= "</div>";
echo $calendar;
?>