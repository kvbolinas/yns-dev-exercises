-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for practice_db
CREATE DATABASE IF NOT EXISTS `practice_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `practice_db`;

-- Dumping structure for table practice_db.quiz_answers
CREATE TABLE IF NOT EXISTS `quiz_answers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quiz_question_id` bigint(20) NOT NULL,
  `answer` varchar(150) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- Dumping data for table practice_db.quiz_answers: ~45 rows (approximately)
/*!40000 ALTER TABLE `quiz_answers` DISABLE KEYS */;
INSERT INTO `quiz_answers` (`id`, `quiz_question_id`, `answer`, `is_correct`, `date_created`) VALUES
	(1, 1, 'Brussels', 1, '2018-10-10 16:49:25'),
	(2, 2, 'Canberra', 1, '2018-10-10 16:49:25'),
	(3, 3, 'Manila', 1, '2018-10-10 16:49:25'),
	(4, 4, 'Riga', 1, '2018-10-10 16:49:25'),
	(5, 5, 'Islambad', 1, '2018-10-10 16:49:25'),
	(6, 6, 'Sanaa', 1, '2018-10-10 16:49:25'),
	(7, 7, 'Kiev', 1, '2018-10-10 16:49:25'),
	(8, 8, 'Yerevan', 1, '2018-10-10 16:49:25'),
	(9, 9, 'San Jose', 1, '2018-10-10 16:54:14'),
	(10, 10, 'Skopje', 1, '2018-10-10 16:54:14'),
	(11, 11, 'Apia', 1, '2018-10-10 16:54:14'),
	(12, 12, 'Ashgabat', 1, '2018-10-10 16:54:14'),
	(13, 13, 'Luanda', 1, '2018-10-10 16:54:14'),
	(14, 14, 'Tokyo', 1, '2018-10-10 16:54:14'),
	(15, 15, 'Stockholm', 1, '2018-10-10 16:54:14'),
	(16, 1, 'Bruges', 0, '2018-10-10 16:49:25'),
	(17, 1, 'Antwerp', 0, '2018-10-10 16:49:25'),
	(18, 2, 'Sydney', 0, '2018-10-10 16:49:25'),
	(19, 2, 'Melbourne', 0, '2018-10-10 16:49:25'),
	(20, 3, 'Cebu', 0, '2018-10-10 16:49:25'),
	(21, 3, 'Davao', 0, '2018-10-10 16:49:25'),
	(22, 4, 'Sigulda', 0, '2018-10-10 16:49:25'),
	(23, 4, 'Jurmala', 0, '2018-10-10 16:49:25'),
	(24, 5, 'Karachi', 0, '2018-10-10 16:49:25'),
	(25, 5, 'Lahore', 0, '2018-10-10 16:49:25'),
	(26, 6, 'Taiz', 0, '2018-10-10 16:49:25'),
	(27, 6, 'Aden', 0, '2018-10-10 16:49:25'),
	(28, 7, 'Lviv', 0, '2018-10-10 16:49:25'),
	(29, 7, 'Odessa', 0, '2018-10-10 16:49:25'),
	(30, 8, 'Gyumri', 0, '2018-10-10 16:49:25'),
	(31, 8, 'Vagharshapat', 0, '2018-10-10 16:49:25'),
	(32, 9, 'Monte Verde', 0, '2018-10-10 16:54:14'),
	(33, 9, 'San Carlos', 0, '2018-10-10 16:54:14'),
	(34, 10, 'Kumanovo', 0, '2018-10-10 16:54:14'),
	(35, 10, 'Bitola', 0, '2018-10-10 16:54:14'),
	(36, 11, 'Savaii', 0, '2018-10-10 16:54:14'),
	(37, 11, 'Upolu', 0, '2018-10-10 16:54:14'),
	(38, 12, 'Dasoguz', 0, '2018-10-10 16:54:14'),
	(39, 12, 'Turkmenabat', 0, '2018-10-10 16:54:14'),
	(40, 13, 'Lubango', 0, '2018-10-10 16:54:14'),
	(41, 13, 'Lobito', 0, '2018-10-10 16:54:14'),
	(42, 14, 'Kyoto', 0, '2018-10-10 16:54:14'),
	(43, 14, 'Osaka', 0, '2018-10-10 16:54:14'),
	(44, 15, 'Gothenburg', 0, '2018-10-10 16:54:14'),
	(45, 15, 'Gotland', 0, '2018-10-10 16:54:14');
/*!40000 ALTER TABLE `quiz_answers` ENABLE KEYS */;

-- Dumping structure for table practice_db.quiz_questions
CREATE TABLE IF NOT EXISTS `quiz_questions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(250) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table practice_db.quiz_questions: ~15 rows (approximately)
/*!40000 ALTER TABLE `quiz_questions` DISABLE KEYS */;
INSERT INTO `quiz_questions` (`id`, `question`, `date_created`) VALUES
	(1, 'What is the capital of Belgium?', '2018-10-10 16:41:36'),
	(2, 'What is the capital of Australia?', '2018-10-10 16:41:56'),
	(3, 'What is the capital of Philippines?', '2018-10-10 16:41:59'),
	(4, 'What is the capital of Latvia?', '2018-10-10 16:42:00'),
	(5, 'What is the capital of Pakistan?', '2018-10-10 16:42:01'),
	(6, 'What is the capital of Yemen?', '2018-10-10 16:42:01'),
	(7, 'What is the capital of Ukraine?', '2018-10-10 16:42:02'),
	(8, 'What is the capital of Armenia?', '2018-10-10 16:42:02'),
	(9, 'What is the capital of Costa Rica?', '2018-10-10 16:42:02'),
	(10, 'What is the capital of Macedonia', '2018-10-10 16:42:03'),
	(11, 'What is the capital of Samoa?', '2018-10-10 16:42:03'),
	(12, 'What is the capital of Turkmenistan?', '2018-10-10 16:42:04'),
	(13, 'What is the capital of Angola?', '2018-10-10 16:42:04'),
	(14, 'What is the capital of Japan?', '2018-10-10 16:42:05'),
	(15, 'What is the capital of Sweden?', '2018-10-10 16:42:07');
/*!40000 ALTER TABLE `quiz_questions` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
